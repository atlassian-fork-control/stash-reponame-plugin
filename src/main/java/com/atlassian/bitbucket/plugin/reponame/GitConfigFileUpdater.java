package com.atlassian.bitbucket.plugin.reponame;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class GitConfigFileUpdater implements ConfigFileUpdater {

    public static final String PROJECT_KEY = "bitbucket.project";
    public static final String REPO_SLUG = "bitbucket.repository";

    private static final Logger log = LoggerFactory.getLogger(GitConfigFileUpdater.class);

    private final GitCommandBuilderFactory builderFactory;

    public GitConfigFileUpdater(GitCommandBuilderFactory builderFactory) {
        this.builderFactory = builderFactory;
    }

    public void updateConfigFile(final Repository repository) throws IOException {
        if ("git".equals(repository.getScmId())) {
	    log.info("Updating project key and repository name: {}/{}", repository.getProject().getKey(), repository.getSlug());
            builderFactory.builder(repository).config().set(PROJECT_KEY, repository.getProject().getKey()).build().call();
            builderFactory.builder(repository).config().set(REPO_SLUG, repository.getSlug()).build().call();
        }
    }
}
