package com.atlassian.bitbucket.plugin.reponame;

import com.atlassian.event.api.EventListener;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.atlassian.bitbucket.event.project.ProjectModifiedEvent;
import com.atlassian.bitbucket.event.repository.RepositoryCreatedEvent;
import com.atlassian.bitbucket.event.repository.RepositoryModifiedEvent;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.bitbucket.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

public class RepositoryListener implements PluginUpgradeTask {

    private static final Logger log = LoggerFactory.getLogger(RepositoryListener.class);

    private final ConfigFileUpdater configFileUpdater;
    private final RepositoryService repositoryService;
    private final SecurityService securityService;

    public RepositoryListener(ConfigFileUpdater configFileUpdater,
                              RepositoryService repositoryService,
                              SecurityService securityService) {
        this.configFileUpdater = configFileUpdater;
        this.repositoryService = repositoryService;
        this.securityService = securityService;
    }

    @EventListener
    public void onRepositoryCreated(RepositoryCreatedEvent event) throws IOException {
        // When a new repository is created create the 'name' file
        configFileUpdater.updateConfigFile(event.getRepository());
    }

    @EventListener
    public void onRepositoryModified(RepositoryModifiedEvent event) throws IOException {
        if (isDescriptionChanged(event.getOldValue(), event.getNewValue())) {
            configFileUpdater.updateConfigFile(event.getRepository());
        }
    }

    @EventListener
    public void onProjectModified(final ProjectModifiedEvent event) throws IOException {
        if (isDescriptionChanged(event.getOldValue(), event.getNewValue())) {
            final PageProvider<Repository> projectReposPageProvider = new PageProvider<Repository>() {
                @Override
                public Page<Repository> get(PageRequest pageRequest) {
                    return PageUtils.asPageOf(Repository.class, repositoryService.findByProjectKey(event.getProject().getKey(), pageRequest));
                }
            };
            updateDescriptionFiles(projectReposPageProvider);
        }
    }

    @Override
    public String getPluginKey() {
        return "com.atlassian.bitbucket.server.bitbucket-reponame-plugin";
    }

    @Override
    public int getBuildNumber() {
        return 1;
    }

    @Override
    public String getShortDescription() {
        return "Add Bitbucket project key and repository slug to git config file";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception {
        log.info(getShortDescription());
        final PageProvider<Repository> allReposPageProvider = new PageProvider<Repository>() {
            @Override
            public Page<Repository> get(PageRequest pageRequest) {
                return PageUtils.asPageOf(Repository.class, repositoryService.findAll(pageRequest));
            }
        };
        updateDescriptionFiles(allReposPageProvider);
        return Collections.emptyList();
    }

    private void updateDescriptionFiles(final PageProvider<Repository> pageProvider) throws IOException {
        // We need to run with REPO_READ permissions
        securityService.withPermission(Permission.REPO_READ, "Reading all repositories for their slugs").call(new Operation<Object, IOException>() {
            @Override
            public Object perform() throws IOException {
                Iterable<Repository> repos = new PagedIterable<>(pageProvider, new PageRequestImpl(0, 100));
                for (Repository repository : repos) {
                    configFileUpdater.updateConfigFile(repository);
                }
                return null;
            }
        });
    }

    private static boolean isDescriptionChanged(final Project oldProject, final Project newProject) {
        return !(oldProject.getKey().equals(newProject.getKey()));
    }

    private static boolean isDescriptionChanged(final Repository oldRepo, final Repository newRepo) {
        return isDescriptionChanged(oldRepo.getProject(), newRepo.getProject()) ||
                !oldRepo.getSlug().equals(newRepo.getSlug());
    }
}
