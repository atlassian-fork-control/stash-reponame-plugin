package com.atlassian.bitbucket.plugin.reponame;

import com.atlassian.bitbucket.repository.Repository;

import java.io.IOException;

public interface ConfigFileUpdater {
    void updateConfigFile(final Repository repository) throws IOException;
}
