package com.atlassian.bitbucket.plugin.reponame;

import com.atlassian.bitbucket.event.project.ProjectModifiedEvent;
import com.atlassian.bitbucket.event.repository.RepositoryModifiedEvent;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.user.EscalatedSecurityContext;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.bitbucket.util.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class RepositoryListenerTest {

    private final ConfigFileUpdater configFileUpdater = mock(ConfigFileUpdater.class);
    private final RepositoryService repositoryService = mock(RepositoryService.class);
    private final SecurityService securityService = mock(SecurityService.class);
    private final EscalatedSecurityContext escalatedSecurityContext = mock(EscalatedSecurityContext.class);

    private final RepositoryListener listener = new RepositoryListener(configFileUpdater, repositoryService, securityService);

    @Before
    public void setup() throws Throwable {
        // Handle the code passed to the security service in the Operation
        when(securityService.withPermission(eq(Permission.REPO_READ), anyString())).thenReturn(escalatedSecurityContext);
        when(escalatedSecurityContext.call(any(Operation.class))).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                final Operation operation = (Operation)invocation.getArguments()[0];
                return operation.perform();
            }
        });
    }

    @Test
    public void shouldUpdateConfigFileOnRepoSlugChange() throws Exception {
        final Repository oldRepoValues = repo("KEY", "SLUG");
        final Repository newRepoValues = repo("KEY", "NEWSLUG");
        final RepositoryModifiedEvent event = new RepositoryModifiedEvent(this, oldRepoValues, newRepoValues);

        listener.onRepositoryModified(event);

        verify(configFileUpdater).updateConfigFile(newRepoValues);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldUpdateSubReposOnProjectKeyChange() throws Throwable {
        final Project oldProjectValues = project("KEY");
        final Project newProjectValues = project("NEWKEY");
        final ProjectModifiedEvent event = new ProjectModifiedEvent(this, oldProjectValues, newProjectValues);
        final Repository repo1WithNewProjectKey = repo("NEWKEY", "slug1");
        final Repository repo2WithNewProjectKey = repo("NEWKEY", "slug2");
        given(repositoryService.findByProjectKey(eq("NEWKEY"), any(PageRequest.class))).willReturn((Page) pageOf(repo1WithNewProjectKey, repo2WithNewProjectKey));

        listener.onProjectModified(event);

        verify(configFileUpdater).updateConfigFile(repo1WithNewProjectKey);
        verify(configFileUpdater).updateConfigFile(repo2WithNewProjectKey);
    }

    @Test
    public void shouldNotUpdateConfigFileIfRepoNameIsUnchanged() throws Exception {
        final RepositoryModifiedEvent event = new RepositoryModifiedEvent(this, repo("KEY", "slug"), repo("KEY", "slug"));

        listener.onRepositoryModified(event);

        verifyZeroInteractions(configFileUpdater);
    }

    @Test
    public void shouldNotUpdateConfigFileIfProjectNameIsUnchanged() throws Exception {
        final ProjectModifiedEvent event = new ProjectModifiedEvent(this, project("KEY"), project("KEY"));

        listener.onProjectModified(event);

        verifyZeroInteractions(configFileUpdater);
    }


    private Project project(String projectKey) {
        final Project project = mock(Project.class);
        when(project.getKey()).thenReturn(projectKey);
        return project;
    }

    private Repository repo(String projectKey, String repoSlug) {
        final Repository repository = mock(Repository.class, RETURNS_DEEP_STUBS);
        when(repository.getProject().getKey()).thenReturn(projectKey);
        when(repository.getName()).thenReturn(repoSlug);
        when(repository.getSlug()).thenReturn(repoSlug);
        return repository;
    }

    @SafeVarargs
    private final <T> Page<T> pageOf(T... items) {
        return new PageImpl<>(new PageRequestImpl(0, items.length), items.length, Arrays.asList(items), true);
    }
}