package it.com.atlassian.bitbucket.plugin.reponame;

import com.atlassian.bitbucket.async.AsyncTestUtils;
import com.atlassian.bitbucket.async.WaitCondition;
import com.atlassian.bitbucket.test.*;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import net.sf.json.JSONObject;
import org.hamcrest.Description;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import static com.google.common.base.MoreObjects.firstNonNull;
import static javax.ws.rs.core.Response.Status.CREATED;

public class RepoNameTest {
    private static final Random RANDOM = new Random();

    private RepoSpec repoSpec;
    private String projectKey;

    @Before
    public void createTestProjectAndRepo() throws Exception {
        projectKey = createProject("project");
        repoSpec = createRepo(projectKey, "repo");
    }

    @After
    public void deleteTestProjectAndRepo() throws Exception {
        RepositoryTestHelper.deleteRepository(projectKey, repoSpec.slug);
        ProjectTestHelper.deleteProject(projectKey);
    }

    @Test
    public void shouldSetConfigPropertiesOnExistingRepos() throws Exception {
        // When run with atlas-run/atlas-debug/func-test, Bitbucket comes with one project
        // pre-configured.  This project doesn't currently have the config properties
        // set, so look for them here to see if the upgrade task worked.
        assertConfigFileContaining(1, "PROJECT_1", "rep_1");
    }

    @Test
    public void shouldSetConfigPropertiesWhenCreatingRepo() throws Exception {
        assertConfigFileContaining(repoSpec.id, projectKey, repoSpec.slug);
    }

    @Test
    public void shouldChangeConfigPropertiesWhenRenamingRepo() throws Exception {
        final RepoSpec newRepoSpec = updateRepositorySlug(projectKey, repoSpec.slug, "newrepo");

        assertConfigFileContaining(repoSpec.id, projectKey, newRepoSpec.slug);
    }

    @Test
    public void shouldChangeConfigPropertiesWhenUpdatingProjectKey() throws Exception {
        final String newProjectKey = updateProjectKey(projectKey, "newproject");

        assertConfigFileContaining(repoSpec.id, newProjectKey, repoSpec.slug);
    }

    @Test
    public void shouldChangeConfigPropertiesWhenRepoIsMoved() throws Exception {
        final String newProjectKey = createProject("newproject");
        final RepoSpec newRepoSpec = moveRepository(projectKey, repoSpec.slug, newProjectKey, "newrepo");

        assertConfigFileContaining(repoSpec.id, newProjectKey, newRepoSpec.slug);
    }

    private static void assertConfigFileContaining(final int repoId, final String projectKey, final String repoSlug) throws IOException {
        AsyncTestUtils.waitFor(new WaitCondition() {
            private String actualProjectKey = "";
            private String actualRepoSlug = "";

            @Override
            public boolean test() throws Exception {
                final File bitbucketHome = new File(firstNonNull(System.getenv("BITBUCKET_HOME"), "target/bitbucket/home"));
                final File bitbucketSharedHome = new File(bitbucketHome, "shared");
                final File repoDir = new File(bitbucketSharedHome.isDirectory() ? bitbucketSharedHome : bitbucketHome, String.format("data/repositories/%d", repoId));
                if (repoDir.exists()) {
                    actualProjectKey = getConfigFileValue(repoDir, "bitbucket.project");
                    actualRepoSlug = getConfigFileValue(repoDir, "bitbucket.repository");
                    return projectKey.equals(actualProjectKey) && repoSlug.equals(actualRepoSlug);
                } else {
                    actualProjectKey = actualRepoSlug = String.format("Repo dir does not exist at %s", repoDir.getPath());
                    return false;
                }
            }

            @Override
            public void describeFailure(Description description) throws Exception {
                description.appendText(String.format("Repo config file should contain project key and repo slug in bitbucket section.\n" +
                                                             "\tExpected project key: '%s'   Actual project key: '%s'\n" +
                                                             "\tExpected repo slug:   '%s'   Actual repo slug: '%s'\n",
                                                     projectKey, actualProjectKey, repoSlug, actualRepoSlug));
            }
        }, 2000);
    }

    private static String getConfigFileValue(File repoDir, String key) {
        try {
            return ProcessTestHelper.execute(repoDir, GitTestHelper.getGitPath(), "config", "--get", "--local", key).getStdOut().trim();
        } catch (ProcessFailedException e) {
            // ProcessResultFactory already logs plenty
            return null;
        }
    }

    private static String updateProjectKey(String originalProjectKey, String newProjectKeyBase) {
        final String newProjectKey = newProjectKeyBase + generateRandomString();
        final Response response = ProjectTestHelper.updateProjectKey(originalProjectKey, newProjectKey);
        return getProjectKey(response);
    }

    private static RepoSpec updateRepositorySlug(String projectKey, String originalRepoSlug, String newRepoNameBase) {
        return moveRepository(projectKey, originalRepoSlug, projectKey, newRepoNameBase);
    }

    private static RepoSpec moveRepository(String originalProjectKey, String originalRepoSlug, String newProjectKey, String newRepoNameBase) {
        final String newRepoName = newRepoNameBase + generateRandomString();

        JSONObject newRepoBodyObject = new JSONObject();

        if (!newProjectKey.equals(originalProjectKey)) {
            JSONObject project = new JSONObject();
            project.put("key", newProjectKey);
            newRepoBodyObject.put("project", project);
        }

        newRepoBodyObject.put("name", newRepoName);
        newRepoBodyObject.put("scmId", "git");

        String url = DefaultFuncTestData.getRestURL() + "/projects/" + originalProjectKey + "/repos/" + originalRepoSlug;

        Response response = RestAssured.given()
                .auth().preemptive().basic(DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword())
                .body(newRepoBodyObject.toString()).contentType("application/json")
                .expect().log().ifError().statusCode(CREATED.getStatusCode())
                .when().put(url);

        return getRepoSpec(response);
    }

    private static String createProject(String projectKeyBase) {
        final String proposedProjectKey = projectKeyBase + generateRandomString();
        final Response response = ProjectTestHelper.createProject(proposedProjectKey,
                                                                  proposedProjectKey + " project",
                                                                  proposedProjectKey + " project description");
        // The project key isn't exactly what we sent in, so let Bitbucket tell us what it is
        return getProjectKey(response);
    }

    private static RepoSpec createRepo(String projectKey, String repoNameBase) {
        final String repoName = repoNameBase + generateRandomString();
        final Response response = RepositoryTestHelper.createRepository(projectKey, repoName);
        return getRepoSpec(response);
    }

    private static String getProjectKey(Response projectResponse) {
        final JsonPath jsonProject = JsonPath.from(projectResponse.body().asString());
        return jsonProject.getString("key");
    }

    private static RepoSpec getRepoSpec(Response response) {
        final JsonPath jsonRepo = JsonPath.from(response.body().asString());
        return new RepoSpec(jsonRepo.getInt("id"), jsonRepo.getString("slug"));
    }

    private static String generateRandomString() {
        return Integer.toHexString(RANDOM.nextInt());
    }

    private static class RepoSpec {
        public final int id;
        public final String slug;

        private RepoSpec(int id, String slug) {
            this.id = id;
            this.slug = slug;
        }
    }
}
